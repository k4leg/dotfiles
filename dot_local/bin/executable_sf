#!/usr/bin/env python

"""Sort files in directory."""

import os

import click


@click.command()
@click.argument(
    'dir',
    required=False,
    default='.',
    type=click.Path(exists=True, file_okay=False, resolve_path=True),
)
@click.option('-y/-n', 'confirm', is_flag=True, default=None)
def main(dir, confirm):
    filenames = os.listdir(dir).copy()
    filenames.sort()
    rename = {}
    for n, filename in enumerate(filenames, 1):
        _, ext = os.path.splitext(filename)
        rename[filename] = f'{n}{ext}'
    for src, dst in rename.items():
        print(f"- {src} -> {dst}")
    if not rename:
        print("Nothing to do.")
    elif confirm is None:
        confirm = click.confirm(
            "This is right?", default=False, prompt_suffix=''
        )
    if confirm:
        for src, dst in rename.items():
            os.rename(src, dst)


main()
