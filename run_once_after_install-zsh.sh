#!/usr/bin/env bash

ln -s /usr/share/zsh-theme-powerlevel10k \
    ~/.oh-my-zsh/custom/themes/powerlevel10k \
    || true
