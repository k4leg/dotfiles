call plug#begin()
Plug 'vim-airline/vim-airline', {'commit': 'd0c6777'}

Plug 'preservim/nerdtree', {'commit': '9310f91'}

Plug 'tpope/vim-surround', {'commit': 'aeb9332'}

Plug 'wellle/targets.vim', {'commit': '8d6ff29'}

Plug 'neovim/nvim-lspconfig', {'commit': 'ea29110'}
Plug 'ms-jpq/coq_nvim', {'commit': '81aea6a'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate', 'commit': '1b74dea'}

Plug 'easymotion/vim-easymotion', {'commit': 'd75d959'}

Plug 'tpope/vim-commentary', {'commit': '627308e'}

Plug 'luochen1990/rainbow', {'commit': 'c18071e'}

Plug 'ervandew/supertab', {'commit': 'f0093ae'}

Plug 'honza/vim-snippets', {'commit': '851c39c'}
Plug 'SirVer/ultisnips', {'commit': '2a06971'}

Plug 'KeitaNakamura/tex-conceal.vim', {'commit': '93ae39d'}
Plug 'lervag/vimtex', {'commit': 'b526907'}

Plug 'marko-cerovac/material.nvim', {'commit': 'a7c9069'}

Plug 'baskerville/vim-sxhkdrc', {'for': 'sxhkdrc', 'commit': '7b8abc3'}

Plug 'psf/black', {'commit': 'd038a24'}
call plug#end()

set smartindent
set number relativenumber
set list
set breakindent

autocmd FileType python,tex,lua setlocal expandtab shiftwidth=4
autocmd FileType yaml,nix setlocal expandtab shiftwidth=2
autocmd FileType tex setlocal spell spelllang=ru_ru,en_us textwidth=79

filetype plugin on
filetype indent on
filetype on
syntax on

nmap <ESC> :nohlsearch<CR>
cabbrev W w!
cabbrev Q q!
inoremap <C-l> <C-g>u<ESC>[s1z=`]a<C-g>u

set langmap+=фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
set langmap+=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ
set langmap+=ЖжЭэХхЪъБЮю;\:\;\"\'{[}]<>.

" Plugins
"" preservim/nerdtree
nmap <F2> :NERDTreeToggle<CR>

"" neovim/nvim-lspconfig
lua << EOF
  local nvim_lsp = require('lspconfig')
  local on_attach = function(client, bufnr)
    local function buf_set_keymap(...)
      vim.api.nvim_buf_set_keymap(bufnr, ...)
    end
    local function buf_set_option(...)
      vim.api.nvim_buf_set_option(bufnr, ...)
    end

    local opts = { noremap = true, silent = true }

    -- See `:help vim.lsp.*` for documentation on any of the below functions.
    buf_set_keymap('n', 'gD', '<CMD>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gd', '<CMD>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<CMD>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', 'gi', '<CMD>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', 'gr', '<CMD>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', 'gR', '<CMD>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '[d', '<CMD>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
    buf_set_keymap('n', ']d', '<CMD>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  end

  -- Use a loop to conveniently call `setup` on multiple servers and map
  -- buffer local keybindings when the language server attaches.
  local servers = {
    'pylsp', 'clangd', 'bashls', 'texlab', 'hls', 'sumneko_lua'
  }
  for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup {
       on_attach = on_attach,
       flags = { debounce_text_changes = 150 }
    }
  end
  nvim_lsp.cssls.setup {
    on_attach = on_attach,
    flags = { debounce_text_changes = 150 },
    cmd = { "vscode-css-languageserver", "--stdio" }
  }
  nvim_lsp.html.setup {
    on_attach = on_attach,
    flags = { debounce_text_changes = 150 },
    cmd = { "vscode-html-languageserver", "--stdio" }
  }
  nvim_lsp.jsonls.setup {
    on_attach = on_attach,
    flags = { debounce_text_changes = 150 },
    cmd = { "vscode-json-languageserver", "--stdio" }
  }
EOF
nmap gp :bp<CR>
nmap gn :bn<CR>
nmap ge :bd<CR>

"" ms-jpq/coq_nvim
set completeopt=menuone,noinsert,noselect
set shortmess+=c
let g:coq_settings = {'auto_start': 'shut-up', 'keymap.recommended': v:false}
lua local coq = require('coq')

"" nvim-treesitter/nvim-treesitter
lua << EOF
  require('nvim-treesitter.configs').setup {
    ensure_installed = {
      'bash', 'c', 'comment', 'cpp', 'css', 'dockerfile', 'html', 'json',
      'latex', 'python', 'regex', 'toml', 'yaml', 'lua'
    },
    highlight = { enable = true },
  }
EOF
set foldlevel=1000000 foldmethod=expr foldexpr=nvim_treesitter#foldexpr()

"" easymotion/vim-easymotion
let g:EasyMotion_leader_key = '<SPACE>'

"" tpope/vim-commentary
autocmd FileType c,cpp setlocal commentstring=//\ %s

"" luochen1990/rainbow
let g:rainbow_active = v:true

"" ervandew/supertab
let g:SuperTabDefaultCompletionType = '<C-n>'
let g:SuperTabContextDefaultCompletionType = '<C-n>'
let g:SuperTabMappingTabLiteral = '<C-t>'

"" SirVer/ultisnips
let g:UltiSnipsExpandTrigger = '<TAB>'
let g:UltiSnipsJumpForwardTrigger = '<TAB>'
let g:UltiSnipsJumpBackwardTrigger = '<S-TAB>'

"" KeitaNakamura/tex-conceal.vim
set conceallevel=1
let g:tex_conceal = 'abdmg'
hi Conceal ctermbg=none

"" lervag/vimtex
let g:tex_flavor = 'latex'
let g:vimtex_quickfix_mode = v:false
let g:vimtex_view_method = 'zathura'

"" marko-cerovac/material.nvim
let g:material_style = 'darker'
lua << EOF
  require('material').setup({
    italics = { comments = true },
    disable = { background = true },
  })
EOF
colorscheme material

"" psf/black
let g:black_linelength = 79
