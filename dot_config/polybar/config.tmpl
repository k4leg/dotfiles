; vim:ft=dosini

[colors]
background = #222
background-alt = #444
foreground = #dfdfdf
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/top]
dpi = ${xrdb:Xft.dpi:-1}
width = 100%
height = 37
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 2

font-0 = {{ .font.interface }}:size=11:weight=bold;1
font-1 = {{ .font.monospace }}:size=10:weight=bold;1

modules-left = bspwm
modules-right = pulseaudio xkeyboard cpu_temperature gpu_temperature wifi_temperature date

tray-position = right
tray-padding = 2

wm-restack = bspwm

cursor-click = pointer
cursor-scroll = ns-resize

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

[module/date]
type = internal/date
interval = 0.1

date = %a %b  %d %T %Z %Y

format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #0a6cf5

label = %date%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = 🔇 muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/cpu_temperature]
type = internal/temperature
hwmon-path = /sys/devices/pci0000:00/0000:00:18.3/hwmon/hwmon0/temp1_input
thermal-zone = 0
warn-temperature = 60

format = CPU <label>
format-underline = #f50a4d
format-warn = CPU <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

[module/gpu_temperature]
type = internal/temperature
hwmon-path = /sys/devices/pci0000:00/0000:00:08.1/0000:06:00.0/hwmon/hwmon3/temp1_input
thermal-zone = 0
warn-temperature = 60

format = GPU <label>
format-underline = #f50a4d
format-warn = GPU <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

[module/wifi_temperature]
type = internal/temperature
hwmon-path = /sys/devices/virtual/thermal/thermal_zone0/hwmon2/device/hwmon2/temp1_input
thermal-zone = 0
warn-temperature = 60

format = Wi-Fi <label>
format-underline = #f50a4d
format-warn = Wi-Fi <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

[settings]
screenchange-reload = true

[global/wm]
margin-top = 5
margin-bottom = 0
